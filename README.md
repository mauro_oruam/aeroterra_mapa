# Mapa Interactivo 

Este proyecto fue realizado para el desafío de Aeroterra. 
Consiste en un mapa en el que se pueden cargar puntos de interés con su información relevante.


## Comenzando

Clona el repositorio desde gitlab.com a tu espacio de trabajo:

$ git clone git@gitlab.com:mauro_oruam/aeroterra_mapa.git

Navega dentro de la carpeta del repositorio:

$ cd aeroterra

Abre el archivo index.html con tu navegador.

En caso de recibir un error de CORS deberás utilizar un servidor http:

$ python3.8 -m http.server

De otro modo no ser cargarán los datos del archivo json, pero podrás usar todas las funcionalidades del proyecto sin problema.

## Construído con

* [Javascript](https://www.javascript.com/) 
* [Jquery](https://maven.apache.org/) 
* [Bootstrap](https://rometools.github.io/rome/) 
* [Leaflet](https://leafletjs.com/reference-1.6.0.html) - El framework utilizado para el mapa

## Autor

* **Mauro Agustín Bagnoli** - *más trabajos* - [Portfolio](https://maurobagnoli.github.io/resume)


## Licencia

Este proyecto está bajo una licencia MIT - ver la [licencia](https://opensource.org/licenses/MIT) para mas detalles. 

